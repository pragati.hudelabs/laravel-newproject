<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Formcontroller extends Controller
{
    public function fill()
    {
        $data['title'] = 'hello';
        $data['scr'] = '<script>console.log("hello");</script>';
        return view('pages.form')->with($data);
    }

    public function validateform(Request $request)
    {
        $this->validate($request, ['email' => 'required|email', 'password' => 'required']);
    }
}
