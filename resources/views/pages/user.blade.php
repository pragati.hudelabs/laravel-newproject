@extends("templates.header")

@section("content")
    <h1>New User!</h1>
    @if( isset($list))
        <ul>
            @foreach( $list as $user )
                <li>{{$user}}</li>
            @endforeach
        </ul>
    @else
        <p>No User</p>
    @endif
@endsection("content")